<?php
/**
 * The template for displaying all single posts and attachments
 *
 */
 
get_header(); ?>
 
 <div class="container">
		<div class="row">
			<section id="primary" class="content-area col-sm-12 col-lg-8 offset-lg-2">
				<main id="main" class="site-main" role="main">
                <?php 
						if(function_exists('get_hansel_and_gretel_breadcrumbs')): 
							echo get_hansel_and_gretel_breadcrumbs();
						endif;
					?>
        <?php
        // Start the loop.
        while ( have_posts() ) : the_post();
 
            /*
             * Include the post format-specific template for the content. If you want to
             * use this in a child theme, then include a file called called content-___.php
             * (where ___ is the post format) and that will be used instead.
             */
            get_template_part( 'template-parts/content', 'post' );

            // If comments are open or we have at least one comment, load up the comment template.
            if ( comments_open() || get_comments_number() ) :
                comments_template();
            endif;
 
            // Previous/next post navigation.
            the_post_navigation( array(
                'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next', 'twentyfifteen' ) . '</span> ' .
                    '<span class="screen-reader-text">' . __( 'Next post:', 'twentyfifteen' ) . '</span> ' .
                    '<span class="post-title">%title</span>',
                'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Previous', 'twentyfifteen' ) . '</span> ' .
                    '<span class="screen-reader-text">' . __( 'Previous post:', 'twentyfifteen' ) . '</span> ' .
                    '<span class="post-title">%title</span>',
            ) );
 
        // End the loop.
        endwhile;
        ?>
 
 </div><!-- .navigation -->
				</main><!-- #main -->
			</section><!-- #primary -->
		</div>
	</div>
<?php get_footer(); ?>