<?php
/*
Template Name: Post Archives
*/

get_header(); ?>
<div class="container">
    <div class="row">
        <?php 
            if(function_exists('get_hansel_and_gretel_breadcrumbs')): 
                echo get_hansel_and_gretel_breadcrumbs();
            endif;
        ?>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <h1 class="entry-title"><?php the_title(); ?></h1>
        </div>
    </div>
    <div class="row post-type-archive">
        <?php
        $post_list = get_posts( array(
            'orderby'    => 'menu_order',
            'sort_order' => 'asc'
        ) );

        $posts = array();

        foreach ( $post_list as $post ) {
            $posts[] += $post->ID;
            ?>
            <div class="col-lg-4 blog-style">
                <a href="<?php the_permalink(); ?>" class="item text-center">
                    <div class="post-img">
                        <?php the_post_thumbnail( 'medium'); ?>
                    </div>
                    <div class="item-content">
                        <h5><?php the_title(); ?></h5>
                        <p><?php the_excerpt(); ?></p>
                    </div>
                </a>
            </div>
        <?php
        }
        ?>
    </div>
        
        
</div>
<?php
// get_sidebar();
get_footer();
