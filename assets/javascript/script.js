jQuery(function($) {

    // Add custom script here. Please backup the file before editing/modifying. Thanks
    
    // Run the script once the document is ready
    $(document).ready(function() {
        var body = document.querySelector('body');
        var bodyheight = body.offsetHeight+"px";
        var bg = document.querySelector('#bg');
        if(bg) {
            bg.style.height = bodyheight;
        }
    });

    // Run the script once the window finishes loading
    $(window).load(function(){
        
    });


});
