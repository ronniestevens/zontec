<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>
	
	<div class="container">
		<div class="row">
		<?php 
			if(function_exists('get_hansel_and_gretel_breadcrumbs')): 
				echo get_hansel_and_gretel_breadcrumbs();
			endif;
		?>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<h1 class="entry-title">Referenties</h1>
			</div>
		</div>
		<div class="row post-archive">
			<?php
			// WP_Query arguments
			$args = array (
				'post_type'              => array( 'projecten' ),
				'post_status'            => array( 'publish' ),
				'nopaging'               => true,
				'order'                  => 'ASC',
				'orderby'                => 'menu_order',
			);

			// The Query
			$projects = new WP_Query( $args );
			$numbr = 0;

			// The Loop
			if ( $projects->have_posts() ) {
				while ( $projects->have_posts() ) {
					++$numbr;
						
					$projects->the_post();
					$m_meta_plaats = get_post_meta($post->ID, 'meta-box-plaats', true);
					$m_meta_soortbedrijf = get_post_meta($post->ID, 'meta-box-soortbedrijf', true);
					$m_meta_omvang = get_post_meta($post->ID, 'meta-box-omvang', true);
					$m_meta_referentie = get_post_meta($post->ID, 'meta-box-referentie', true);
					?>
					<div class="col-lg-4 blog-style">
						<a href="<?php echo get_permalink(); ?>" class="item text-center">
							
							<div class="post-img">
								<?php the_post_thumbnail('project-archive'); ?>
							</div>
							<div class="item-content">
								<h5><?php the_title(); ?></h5>

								<?php 
									$excerpt = '';
									if (has_excerpt()) {
										$excerpt = wp_strip_all_tags(get_the_excerpt());
									}
								?>
								<dl class="meta row">
									<?php
										if($m_meta_plaats) {
											echo '<dt class="col-md-4">Plaats:</dt><dd class="col-md-8"> ' . $m_meta_plaats . '</dd>';
										}
										if($m_meta_soortbedrijf) {
											echo '<dt class="col-md-4">Soort Bedrijf:</dt><dd class="col-md-8"> ' . $m_meta_soortbedrijf . '</dd>';
										}
										if($m_meta_omvang) {
											echo '<dt class="col-md-4">Omvang:</dt><dd class="col-md-8"> ' . $m_meta_omvang . '</dd>';
										}

									?>
								</dl>
							</div>
							<div class="readmore">
								lees verder...
								</div>

						</a>
					</div>
					<?php
					}							
				} else {
					// no posts found
					echo "no posts found";
				}
				// Restore original Post Data
				wp_reset_postdata();
			?>		
		</div><!-- .row -->
	</div><!-- .container -->

<?php
// get_sidebar();
get_footer();
