<?php

function homeprojects() {
// WP_Query arguments
    $args = array (
        'post_type'              => array( 'projecten' ),
        'post_status'            => array( 'publish' ),
        'nopaging'               => true,
        'order'                  => 'ASC',
        'orderby'                => 'menu_order',
    );

    // The Query
    $services = new WP_Query( $args );

    // The Loop
    if ( $services->have_posts() ) {
        while ( $services->have_posts() ) {
            $services->the_post();
            // do something
            $title = the_title();
        }
    } else {
        // no posts found
        echo "no posts found";
    }

    // Restore original Post Data
    wp_reset_postdata();
    return title;
}

?>