<?php

// Include php files
include get_theme_file_path('/includes/shortcodes.php');

// Include custom nav-walker
include get_theme_file_path('/inc/wp_bootstrap_navwalker.php');


// Include the breadcrumbs 
include_once('breadcrumbs.php');

// Enqueue needed scripts
function needed_styles_and_scripts_enqueue() {
    wp_enqueue_script( 'wpbs-custom-script', get_stylesheet_directory_uri() . '/assets/javascript/script.js' , array( 'jquery' ) );
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' ,100);
}
add_action( 'wp_enqueue_scripts', 'needed_styles_and_scripts_enqueue' );


function zooprefix_enqueue_custom_scripts() { 
    wp_enqueue_style( 'Roboto' , 'https://fonts.googleapis.com/css?family=Kalam:400&display=swap');
}
add_action( 'wp_enqueue_scripts', 'zooprefix_enqueue_custom_scripts' ); 

function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}

// misc support options
add_filter('upload_mimes', 'cc_mime_types');
add_post_type_support( 'page', 'excerpt' );
add_filter( 'widget_text', 'do_shortcode' );
add_image_size( 'homepage-slide', 600, 500 );

//Dynamic Year
function site_year(){
	ob_start();
	echo date( 'Y' );
	$output = ob_get_clean();
    return $output;
}
add_shortcode( 'site_year', 'site_year' );

//
// Your code goes below
//

add_image_size( 'project-thumbnail', 590, 590 );
add_image_size( 'project-archive', 300, 9999, false);
/* banner content type */

function zooslides_init() {
    $args = array(
        'label' => "Zoo slides",
        'public' => true,
        'supports' => array(
            'title',
            'thumbnail',
        )
        );
        register_post_type( 'zooslides', $args );
}
add_action( 'init' , 'zooslides_init' );


/**
 * Homepage widgets
 */


 function home1_section() {
    $args = array(
        'id' => 'home1-widget',
        'name' => __( 'Home one widget', 'text_domain'),
        'description' => __( 'First home widget.', 'text_domain'),
        'before_title' => '<h3 class="widget_title">',
        'after_title' => '</h3>',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
    );
    register_sidebar( $args );
}
add_action ('widgets_init', 'home1_section' );

function home2_section() {
    $args = array(
        'id' => 'home2-widget',
        'name' => __( 'Home two widget', 'text_domain'),
        'description' => __( 'Second home widget.', 'text_domain'),
        'before_title' => '<h3 class="widget_title">',
        'after_title' => '</h3>',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
    );
    register_sidebar( $args );
}
add_action ('widgets_init', 'home2_section' );

function home3_section() {
    $args = array(
        'id' => 'home3-widget',
        'name' => __( 'Home three widget', 'text_domain'),
        'description' => __( 'Third home widget.', 'text_domain'),
        'before_title' => '<h3 class="widget_title">',
        'after_title' => '</h3>',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
    );
    register_sidebar( $args );
}
add_action ('widgets_init', 'home3_section' );

function home4_section() {
    $args = array(
        'id' => 'home4-widget',
        'name' => __( 'Home four widget', 'text_domain'),
        'description' => __( 'Fourth home widget.', 'text_domain'),
        'before_title' => '<h3 class="widget_title">',
        'after_title' => '</h3>',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
    );
    register_sidebar( $args );
}
add_action ('widgets_init', 'home4_section' );


function home5_section() {
    $args = array(
        'id' => 'home5-widget',
        'name' => __( 'Home five widget', 'text_domain'),
        'description' => __( 'Fifth home widget.', 'text_domain'),
        'before_title' => '<h3 class="widget_title">',
        'after_title' => '</h3>',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
    );
    register_sidebar( $args );
}
add_action ('widgets_init', 'home5_section' );

function home6_section() {
    $args = array(
        'id' => 'home6-widget',
        'name' => __( 'Home six widget', 'text_domain'),
        'description' => __( 'Sixth home widget.', 'text_domain'),
        'before_title' => '<h3 class="widget_title">',
        'after_title' => '</h3>',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
    );
    register_sidebar( $args );
}
add_action ('widgets_init', 'home6_section' );

function home7_section() {
    $args = array(
        'id' => 'home7-widget',
        'name' => __( 'Home seven widget', 'text_domain'),
        'description' => __( 'Seventh home widget.', 'text_domain'),
        'before_title' => '<h3 class="widget_title">',
        'after_title' => '</h3>',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
    );
    register_sidebar( $args );
}
add_action ('widgets_init', 'home7_section' );



function meks_which_template_is_loaded() {
	if ( is_super_admin() ) {
			global $template;
			print_r( $template );
	}
}
add_action( 'wp_footer', 'meks_which_template_is_loaded' );


function homeslider () {
    $args = array (
        'post_type'              => array( 'projecten' ),
        'post_status'            => array( 'publish' ),
        'nopaging'               => true,
        'order'                  => 'ASC',
        'orderby'                => 'menu_order',
        'meta_query' => array(
			array(
				'key' => 'show-on-homepage',
				'value' => 1,
            )
        ),
    );

    // The Query
    $projects = new WP_Query( $args );
    $numbr = 0;

    // The Loop
    if ( $projects->have_posts() ) {?>
        
        <div class="carousel-inner">
            <?php
                while ( $projects->have_posts() ) {
                    ++$numbr;
                
                    $projects->the_post();
                    $m_meta_plaats = get_post_meta(get_the_ID(), 'meta-box-plaats', true);
                    $m_meta_soortbedrijf = get_post_meta(get_the_ID(), 'meta-box-soortbedrijf', true);
                    $m_meta_omvang = get_post_meta(get_the_ID(), 'meta-box-omvang', true);
                    $m_meta_referentie = get_post_meta(get_the_ID(), 'meta-box-referentie', true);
                    $image_url = get_the_post_thumbnail_url(get_the_ID(), 'homepage-slide'); 
                    // do something
                    ?>

                        <div class="carousel-item <?php if($numbr == 1){ echo 'active';}?>">
                            <a href="<?php echo get_permalink(); ?>" class="d-block carousel-content" style="background-image:url('<?php echo $image_url?>')">
                                <div class="content-wrapper">
                                    <div id="topwrapper">
                                        <h3><?php the_title(); ?></h3>
                                    </div>
                                    <dl class="meta row">
                                        <?php
                                            if($m_meta_plaats) {
                                                echo '<dt class="col-md-3">Plaats:</dt><dd class="col-md-9"> ' . $m_meta_plaats . '</dd>';
                                            }
                                            if($m_meta_soortbedrijf) {
                                                echo '<dt class="col-md-3">soortbedrijf:</dt><dd class="col-md-9"> ' . $m_meta_soortbedrijf . '</dd>';
                                            }
                                            if($m_meta_omvang) {
                                                echo '<dt class="col-md-3">Omvang:</dt><dd class="col-md-9"> ' . $m_meta_omvang . '</dd>';
                                            }

                                        ?>
                                        <span class="readmore">lees verder...</span>
                                    </dl>
                                </div>
                            </a>
                            <?php
                            ?>
                        </div>
                        <?php
                        }
                        ?>
                        
                    <a class="carousel-control-prev" href="#section2" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#section2" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                
                    <?php
    } else {
        // no posts found
        echo "no posts found";
    }

    // Restore original Post Data
    wp_reset_postdata();

}

function bannerslider() {
    $bargs = array (
        'post_type' => array(' zooslides' ),
        'post_status' => array( 'publish' ),
        'nopaging' => true,
        'order' => 'ASC',
        'orderby' => 'menu_order',
    );

    $slides = new WP_Query( $bargs );
    $count = 0;
    ?>
    <div class="container-fluid" id="bannersection">
        <div class="row section">
            <div class="row section carousel slide" id="banner" data-interval="5000" data-ride="carousel">
                <div class="carousel-inner d-flex">
                    <?php
                    while ( $slides->have_posts() ) : $slides->the_post();
                    $count++;
                    ?>
                    <div class="carousel-item <?php if($count == 1){ echo 'active';}?>">
                        <div class="carousel-content d-flex" style="background-image:url('<?php the_post_thumbnail_url();?>');">
                            <div class="content-wrapper align-self-center">
                                  <h4><?php the_title(); ?></h4>
                            </div>
                        </div>
                    </div>
                    <?php 
                    endwhile; 
                    ?>
                </div>
                <?php if($count > 1 ) { ?>
                    <a class="carousel-control-prev" href="#banner" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#banner" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
            <?php
                }
                // Restore original Post Data
                wp_reset_postdata();

                ?>
            </div>
        </div>
    </div>
</div>
<?php }


function bannervideo() {
    ?>
    <div id="videobanner">
        <video controls="true" autoplay muted loop playsinline height="300" src="/wp-content/themes/zontec/images/ZONTEC_NY-2021.mp4"></video>
    </div>
<?php
}