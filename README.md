# wp-bootstrap-starter-child

Please replace child theme name and directory name depends on what website you are using this. Thanks!

----------
06-04-2019
----------

- Add [site_year] shortcode, dynamically get's the year
- Add /assets/ folder for the custom scripts
- Add /includes/ folder with array helper for custom shortcodes


----------
25-09-2019
----------
Setting up sass:
https://www.elegantthemes.com/blog/tips-tricks/how-to-use-sass-with-wordpress-a-step-by-step-guide

$ sudo apt update
$ sudo apt install ruby-full
$ sudo gem update --system
$ sudo gem install sass
$ sudo gem install compass

create the config.rb file

Make compass watch the project
$ cd /yourproject
$ compass watch