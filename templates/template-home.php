<?php
/**
 * Template Name: Homepage
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WP_Bootstrap_Starter
 */
get_header(); ?>

<div class="container">
	<div class="row section-one flex-container">
		<div class="col-md-4 ">
			<?php dynamic_sidebar( 'home1-widget' ); ?>
		</div>
		<div class="col-md-4 ">
			<?php dynamic_sidebar( 'home2-widget' ); ?>
		</div>
		<div class="col-md-4">
			<?php dynamic_sidebar( 'home3-widget' ); ?>
		</div>

	</div>
	
</div>
<div class="container">
	<div class="row section carousel slide carousel-fade" id="section2" data-ride="carousel">
		<div class="col-12">
			<h3>Referenties</h3>
		</div>	
			<?php
				// WP_Query arguments
				homeslider();
			?>
			
			</div>
		</div>
	</div>
	<div class="container-fluid bordertop">
		<div id="section3">
			<div class="row">
				<div class="col-md-12">
					<?php dynamic_sidebar( 'home4-widget' ); ?>
				</div>	
			</div>
		</div>
		<div class="container">
			<div id="section4">
			<div class="row">
				<div class="col-md-4">
					<?php dynamic_sidebar( 'home5-widget' ); ?>
				</div>
				<div class="col-md-4">
					<?php dynamic_sidebar( 'home6-widget' ); ?>
				</div>
				<div class="col-md-4">
					<?php dynamic_sidebar( 'home7-widget' ); ?>
				</div>
			</div>
			</div>
		</div>
	</div>
<?php
get_footer();
