<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WP_Bootstrap_Starter
 */

get_header('projecten'); ?>
	<div class="container">
		<div class="row">
	<section id="primary" class="content-area col-sm-12">
		<main id="main" class="site-main" role="main">
			<div class="row">
				<div class="col-md-6 col-12">
					<?php 
						if(function_exists('get_hansel_and_gretel_breadcrumbs')): 
							echo get_hansel_and_gretel_breadcrumbs();
						endif;
					?>
						<!-- contents of Your Post -->
						<h1><?php the_title(); ?></h1>
						<dl class="meta row">
						
						<?php
							$m_meta_plaats = get_post_meta($post->ID, 'meta-box-plaats', true);
							$m_meta_soortbedrijf = get_post_meta($post->ID, 'meta-box-soortbedrijf', true);
							$m_meta_omvang = get_post_meta($post->ID, 'meta-box-omvang', true);
							$m_meta_referentie = get_post_meta($post->ID, 'meta-box-referentie', true);

								if($m_meta_plaats) {
									echo '<dt class="col-md-4">Plaats:</dt><dd class="col-md-8"> ' . $m_meta_plaats . '</dd>';
								}
								if($m_meta_soortbedrijf) {
									echo '<dt class="col-md-4">Soort Bedrijf:</dt><dd class="col-md-8"> ' . $m_meta_soortbedrijf . '</dd>';
								}
								if($m_meta_omvang) {
									echo '<dt class="col-md-4">Omvang:</dt><dd class="col-md-8"> ' . $m_meta_omvang . '</dd>';
								}

						?>
						</dl>

						<?php
							// TO SHOW THE PAGE CONTENTS
							while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
								<div class="entry-content-page">
									<?php the_content(); ?> <!-- Page Content -->
								</div><!-- .entry-content-page -->

							<?php
							endwhile; //resetting the page loop
							wp_reset_query(); //resetting the page query
							?>					
				</div>
				<div class="col-md-6 col-12 thumb">
					<?php the_post_thumbnail( 'project-thumbnail' ); ?>
					
				</div><!-- end col -->
			</div><!-- end row -->
		



		</main><!-- #main -->
	</section><!-- #primary -->
			</div>
			</div>
<?php
// get_sidebar();
get_footer();
